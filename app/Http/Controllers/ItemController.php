<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddItemToReadingsRequest;
use App\Models\Item;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|Response
     */
    public function index()
    {
        $items = Item::where('user_id', auth()->user()->id)->get();
        return view('items.index', compact('items'));
    }

    /**
     * @param AddItemToReadingsRequest $request
     * @return JsonResponse
     */
    public function store(AddItemToReadingsRequest $request): JsonResponse
    {
        if (Auth::check()) {
            $item = new Item();
            $item->book_id = $request->get('book_id');
            $item->user_id = auth()->user()->id;
            $item->save();
            return response()->json(['readings' => 'html'], 201);
        }
        return response()->json(['redirect' => route('login')], 302);
    }
}

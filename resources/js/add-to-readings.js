$(document).ready(
    $('.add-to-readings').on('click', function (e) {
        e.preventDefault();
        const bookId = $(this).data('book-id');
        const token = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            method: 'POST',
            url: '/items',
            data: {'_token': token, 'book_id': bookId}
        }).done(response => {
            console.log(response)

        }).fail(response => {
            if (response.status === 302) {
                window.location = response.responseJSON.redirect
            }
        })
        console.log($(this).data('book-id'))
    })
);

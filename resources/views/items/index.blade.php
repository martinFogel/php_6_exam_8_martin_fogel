@extends('layouts.app')

@section('content')
    <table class="table" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование</th>
            <th scope="col">Статус</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <th scope="row">{{$item->id}}</th>
                <td>
                    {{$item->book->title}}
                </td>
                <td>
                    статус
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@extends('layouts.app')

@section('content')
    <div style="padding-bottom: 30px;">
        <h3>Книги в жанре "{{$genre->name}}"</h3>
    </div>
    <div class="row">
        @foreach($genre->books as $book)
            <div class="card m-3" style="width: 18rem;">
                <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top"
                     style="height: 290px; width: auto" alt="{{asset('/storage/' . $book->picture)}}" width="50px"
                     height="50px">
                <div class="card-body">
                    <h5 class="card-title"><strong>Название:</strong> {{$book->title}}</h5>
                    <h5 class="card-title"><strong>Автор:</strong> {{$book->author}}</h5>
                    <form id="add-book">
                        @csrf
                    <a type="submit" class="btn-outline-primary add-to-readings" data-book-id="{{$book->id}}">Добавить</a>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-auto">
            {{$genre->books->links()}}
        </div>
    </div>
@endsection

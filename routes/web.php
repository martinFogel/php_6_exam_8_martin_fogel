<?php

use App\Http\Controllers\BooksController;
use App\Http\Controllers\GenresController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GenresController::class, 'index'])->name('home');

Route::resource('genres', GenresController::class);
Route::resource('items', \App\Http\Controllers\ItemController::class)->middleware('auth');
Auth::routes();

Route::post('items', [\App\Http\Controllers\ItemController::class, 'store'])->name('items.store');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
